function area = getPixelArea(I)
    gray = rgb2gray(I);
    binary = imbinarize(gray,0.05);
    binary = ~bwareaopen(~binary,100);
    binary = bwareaopen(binary,100);
    area = sum(binary(:));
end

