clear all; close all; clc;

samplesPath = pwd;
cd(samplesPath);
cd('cropped');
sampleDir = dir('*.jpg');
cd('../')
inputs = zeros(24,size(sampleDir,1));
targets = zeros(size(sampleDir,1),1);
for i=1:length(sampleDir)
    fileName = [samplesPath,'/cropped/',sampleDir(i).name];
    I_RGB = imread(fileName);
    feature = Feature_extract(I_RGB);
    inputs(:,i) = feature;
    bin = imbinarize(rgb2gray(I_RGB),0.05);
    bin = sum(bin(:));
    if (bin < 100000)
        if(feature(24)==0)
            targets(i,1) = 7;
        else
            targets(i,1) = 8;
        end
    elseif (bin < 250000)
        if(feature(24)==0)
            targets(i,1) = 5;
        else
            targets(i,1) = 6;
        end    
    elseif (bin < 500000)
        if(feature(24)==0)
            targets(i,1) = 3;
        else
            targets(i,1) = 4;
        end
    elseif (bin < 700000)
        if(feature(24)==0)
            targets(i,1) = 1;
        else
            targets(i,1) = 2;
        end
    end
end
targets = targets';
inputss = [inputs ;targets];
