function [ feature ] = Feature_extract( I_RGB )
    I = I_RGB;
    I_RGB = im2double(I_RGB);

    I_gray = im2double(rgb2gray(I_RGB));
    % mean filter 3*3
    msk = ~conv2(I_gray, ones(3)/9,'same');
    %--------------------------------------------------------------
    %  Color Features
    mean_R = mean(I_RGB(:,:,1))';
    mean_G = mean(I_RGB(:,:,2))';
    mean_B = mean(I_RGB(:,:,3))';
        
    var_R = var(I_RGB(:,:,1))';
    var_G = var(I_RGB(:,:,2))';
    var_B = var(I_RGB(:,:,3))';

    SK_R = skewness(I_RGB(:,:,1))';
    SK_G = skewness(I_RGB(:,:,2))';
    SK_B = skewness(I_RGB(:,:,3))';

    KU_R = kurtosis(I_RGB(:,:,1))';
    KU_G = kurtosis(I_RGB(:,:,2))';
    KU_B = kurtosis(I_RGB(:,:,3))';

    % mean_RGB = [mean_R mean_G mean_B];% concatenate Feateures
    % var_RGB = [var_R var_G var_B];
    % SK_RGB = [SK_R SK_G SK_B];
    % KU_RGB = [KU_R KU_G KU_B];
    mean_RGB = [mean(mean_R) ; mean(mean_G) ; mean(mean_B)];
    var_RGB = [mean(var_R) ; mean(var_G) ; mean(var_B)];
    SK_RGB = [mean(SK_R) ; mean(SK_G) ; mean(SK_B)];
    KU_RGB = [mean(KU_R) ; mean(KU_G) ; mean(KU_B)];
    color_fe = [mean_RGB; var_RGB; SK_RGB; KU_RGB];
    maxV = max(color_fe(:));
    minV = min(color_fe(:));
    Vs = (color_fe - minV) / (maxV - minV);
    %--------------------------------------------------------------
    % Shape and Size Features
    I_msk = im2bw(~msk);  % be sure your image is binary
    I_msk = imdilate(I_msk , strel('disk',10));
%     figure;imshow(I_msk);
    L = bwlabel(I_msk); % label each object
    s = regionprops(L==1, 'Area','Eccentricity','EquivDiameter','MajorAxisLength','MinorAxisLength','Perimeter','Solidity');
    shap_fe = [s.Area ; s.Eccentricity ; s.EquivDiameter ; s.MajorAxisLength ; s.MinorAxisLength ; s.Perimeter ; s.Solidity]; 
    maxV = max(shap_fe(:));
    minV = min(shap_fe(:));
    Ss = (shap_fe - minV) / (maxV - minV);
    %--------------------------------------------------------------
    % Texture Features
    glcm = graycomatrix(I_gray);
    stats = graycoprops(glcm);
    texture_fe = [stats.Contrast ; stats.Correlation ; stats.Energy ; stats.Homogeneity];
    % Feature Vector
%     feature = [Vs ; Ss ; texture_fe];

    %--------------------------------------------------------------
    % Defection percentage
    bin = imbinarize(rgb2gray(I),0.05);
    bin = sum(bin(:));
    mask = defectionMask(I);
    mask = sum(mask(:));
    defection = (mask/bin) * 100;
    feature = [color_fe ; shap_fe ; texture_fe ; defection ];
    
end

