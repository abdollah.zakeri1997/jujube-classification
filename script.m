% Read image into the workspace.
% mkdir('categorized/100');
% mkdir('categorized/200');
% mkdir('categorized/300');
% mkdir('categorized/400');
% mkdir('categorized/500');
% mkdir('categorized/600');
% mkdir('categorized/700');



clc;
clear all;
close all;




I = imread('newSelected/IMG_2366.JPG');
gray = rgb2gray(I);
binary = imbinarize(gray,0.30);

se = strel('disk',3);

opened = imopen(binary,se);
Iobr = imreconstruct(opened,binary);
closed = imclose(Iobr,se);
Icbr = imreconstruct(closed,Iobr);
bwopened = ~bwareaopen(~Icbr,5);

Ld = watershed(~bwopened);
labels = label2rgb(Ld);
fuse = imfuse(bwopened,labels);
fuse = insertText(fuse,[100 300],numel(regionprops(Ld)),'FontSize',150,'AnchorPoint','LeftBottom');
montaged = fuse;
%imwrite(montaged,['wrinkle/' sampleDir(i).name]);
imshow(montaged);









I = imread('new/DSC_1105.jpg');
gray = rgb2gray(I);
bin = imbinarize(gray,0.4);
figure;
imshow(bin);


%shadow removal and pre processing
I = imread('cropped/IMG_1417.JPG.jpg');
I = im2double(I);
bin = imbinarize(rgb2gray(I),0.9);
figure;
bin = imcomplement(bin);
I = I .* bin;
I(I==0) = NaN;
imshow(inpaint_nans(I));
%colors(I);
showAll(I,11);
original = I;
I = im2double(I);

mask = shadowMask(I);
I = I .* mask;
I = imerode(I,strel('disk',5));
I = imclose(I , strel('disk',25));

I = im2double(I);
I = imerode(I,strel('disk',25));
I = imdilate(I,strel('disk',15));
I = imfilter(I,fspecial('gaussian'));
gray = rgb2gray(I);

I = imbinarize(gray,0.1);
I = im2double(original) .* I;
gray = rgb2gray(I);
%I = imadjust(gray);
I = gray;
%binary = imbinarize(I,0.75);

binary = imbinarize(I,0.4);

se = strel('disk',10);

opened = imopen(binary,se);
Iobr = imreconstruct(opened,binary);
closed = imclose(Iobr,se);
Icbr = imreconstruct(closed,Iobr);

binary = imcomplement(Icbr);
dist = bwdist(binary);
figure;
imshow(dist);

Ld = watershed(binary);

labels = label2rgb(Ld);
fuse = imfuse(binary,label2rgb(Ld));
fuse = insertText(fuse,[100 300],numel(regionprops(Ld)),'FontSize',150,'AnchorPoint','LeftBottom');
figure;
imshow([fuse original]);


%watershed

se = strel('disk',20);
opened = imopen(I,se);
Iobr = imreconstruct(opened,I);
closed = imclose(Iobr,se);
Icbr = imreconstruct(closed,Iobr);
gray = rgb2gray(Icbr);
figure;
imshow(imregionalmin(gray));
ultimate = bwulterode(gray);
dilated = imdilate(ultimate,strel('disk',3));
Ld = watershed(dilated);



figure;
for k = 30 : 38
    binary = imbinarize(rgb2gray(I),k/100);
    subplot(3,3,k-30+1);
    imshow(binary);
end



% showAll(I,20);
% gray = rgb2gray(I);
% gray = imadjust(gray);
% figure;
% subplot(2,2,1);
% imshow(gray);
% 
% se = strel('disk',15);
% Ie = imerode(gray,se);
% Iobr = imreconstruct(Ie,gray);
% subplot(2,2,2);
% imshow(Iobr);
% 
% fgm = imregionalmax(Iobr);
% fgm = ~bwareaopen(~fgm, 50);
% subplot(2,2,3);
% imshow(imcomplement(fgm));
% 
% Irgb = label2rgb(watershed(fgm));
% figure;
% imshow(imfuse(fgm,Irgb));
% 
% 
% figure;
% bw = imbinarize(gray,0.05);
% bw2 = ~bwareaopen(~bw, 10);
% figure;
% imshow(bw2);
% D = -bwdist(~bw);
% figure;
% imshow(D,[]);
% Ld = watershed(D);
% figure;
% imshow(label2rgb(Ld));



Ibw = ~im2bw(rgb2gray(I),graythresh(I));
Ifill = imfill(Ibw,'holes');
Iarea = bwareaopen(Ifill,100);
Ifinal = bwlabel(Iarea);
stat = regionprops(Ifinal,'boundingbox');
imshow(I); hold on;
max = [0 0 0 0];
for cnt = 1 : numel(stat)
    bb = stat(cnt).BoundingBox;
    rectangle('position',bb,'edgecolor','r','linewidth',2);
    if (bb(3) + bb(4) > max(3) + max(4))
        max = bb;
    end
end


imshow(imcrop(I,max));

% figure;
% mask = shadowMask(a);
% a = a.* (~mask);
% imshow(a);

figure;
subplot(2,2,1);
imshow(a);

filter = fspecial('sobel');
mode = 'circular';
h = imfilter(a,filter,mode);
v = imfilter(a,filter',mode);
edged = sqrt(h.^2 + v.^2);
subplot(2,2,2);
imshow(edged);
%imwrite(imadjust(,'results/1531_adjust.jpg');

% filter = fspecial('log',10,1);
% mode = 'circular';
% edged = a + -1 * imfilter(a,filter,mode);
% subplot(2,2,2);
% imshow(edged);

thresholded = rgb2gray(edged) > ( 0.5 );
subplot(2,2,3);
imshow(thresholded);

% dialated = imdilate(thresholded,strel('disk',5));
% subplot(2,2,4);
% imshow(dialated);
% denoised = imerode(treshholded,strel('disk',1));

% denoised = imfilter(thresholded,fspecial('disk',),mode);
denoised = imfilter(thresholded,fspecial('average',[10 10]),mode);

% for k = 1:10
%     denoised = imfilter(denoised,fspecial('average',[1 1]),mode);
% end


subplot(2,2,4);
imshow(denoised);

im

%  s = regionprops(treshholded, 'Centroid');
% 
%  hold on
%  
%  for k = 1:numel(s)
%      c = s(k).Centroid;
%      text(c(1), c(2), sprintf('%d', k), ...
%          'HorizontalAlignment', 'center', ...
%          'VerticalAlignment', 'middle');
%  end
%  hold off
%  
% subplot(2,2,4);
% imshow(treshholded)

% 
samplesPath = [pwd,'/cropped/'];
cd(samplesPath);
sampleDir = dir('*.jpg');
cd('../');
for i=1:length(sampleDir)
    fileName = ['cropped/',sampleDir(i).name];
    I = imread(fileName);
    
    I = imread('new/IMG_2362.jpg');
    
    %colors(I);
    original = I;
    I = im2double(I);
%     I = imerode(I,strel('disk',5));
%     I = imclose(I , strel('disk',25));
%     figure;
%     imshow(I);
%     I = im2double(I);
%     I = imerode(I,strel('disk',25));
%     I = imdilate(I,strel('disk',15));
%     I = imfilter(I,fspecial('gaussian'));
%     gray = rgb2gray(I);
%     figure;
%     imshow(I);
    I = imbinarize(gray,0.1);
    I = im2double(original) .* I;
    gray = rgb2gray(I);
    
    I = gray;
    
    I(I>=0.8) = I(I>=0.8) - 0.4;
    I = medfilt2(I,[5 5]);
    figure;
    imshow(I);
    binary = imbinarize(I,0.4);
        figure;
    imshow(I);
    
    I = rgb2gray(imread('new/DSC_1105.jpg'));
    binary = imbinarize(I,0.40);
    se = strel('disk',5);

    opened = imopen(binary,se);
    Iobr = imreconstruct(opened,binary);
    closed = imclose(Iobr,se);
    Icbr = imreconstruct(closed,Iobr);

    binary = imcomplement(Icbr);
    figure;
    imshow(binary);
    
    Ld = watershed(binary);
    labels = label2rgb(Ld);
    fuse = imfuse(binary,label2rgb(Ld));
    fuse = insertText(fuse,[100 300],numel(regionprops(Ld)),'FontSize',150,'AnchorPoint','LeftBottom');
    montaged = [fuse original];
    %imwrite(montaged,['wrinkle/' sampleDir(i).name]);
    imshow(montaged);
end




%cropping

%     Ibw = ~im2bw(I,graythresh(I));
%     Ifill = imfill(Ibw,'holes');
%     Iarea = bwareaopen(Ifill,100);
%     Ifinal = bwlabel(Iarea);
%     stat = regionprops(Ifinal,'boundingbox');
%     hold on;
%     max = [0 0 0 0];
%     for cnt = 1 : numel(stat)
%         bb = stat(cnt).BoundingBox;
%         rectangle('position',bb,'edgecolor','r','linewidth',2);
%         if ((bb(3) + bb(4) > max(3) + max(4)) && abs(bb(3)-bb(4)) < 20 )
%             max = bb;
%         end
%     end
%     cropped = imcrop(I,max);
%     imwrite(cropped,['cropped/' sampleDir(i).name]);
