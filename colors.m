function colors(I)
    figure;
    r = I(:,:,1);
    g = I(:,:,2);
    b = I(:,:,3);
    subplot(1,3,1);
    imshow(r);
    title('r');
    subplot(1,3,2);
    imshow(g);
    title('g');
    subplot(1,3,3);
    imshow(b);
    title('b');
    
    figure;
    hsv = rgb2hsv(I);
    h = hsv(:,:,1);
    s = hsv(:,:,2);
    v = hsv(:,:,3);
    subplot(1,3,1);
    imshow(h);
    title('h');
    subplot(1,3,2);
    imshow(s);
    title('s');
    subplot(1,3,3);
    imshow(v);
    title('v');
    figure;
    imshow(h + v / 2);
    
    figure;
    lab = rgb2lab(I);
    l = lab(:,:,1);
    a = lab(:,:,2);
    b = lab(:,:,3);
    subplot(1,3,1);
    imshow(l);
    title('l*');
    subplot(1,3,2);
    imshow(a);
    title('a*');
    subplot(1,3,3);
    imshow(b);
    title('b*');
    
    figure;
    lin = rgb2lin(I);
    r = lin(:,:,1);
    g = lin(:,:,2);
    b = lin(:,:,3);
    subplot(1,3,1);
    imshow(r);
    title('r');
    subplot(1,3,2);
    imshow(g);
    title('g');
    subplot(1,3,3);
    imshow(b);
    title('b');
    
    figure;
    YCbCr = rgb2ycbcr(I);
    y = YCbCr(:,:,1);
    cb = YCbCr(:,:,2);
    cr = YCbCr(:,:,3);
    subplot(1,3,1);
    imshow(y);
    title('Y');
    subplot(1,3,2);
    imshow(cb);
    title('Cb');
    subplot(1,3,3);
    imshow(cr);
    title('Cr');
    
    figure;
    imshow(y + cr / 2);
    
end

