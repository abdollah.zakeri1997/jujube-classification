I = imread('samples/1.JPG');

I = im2bw(I);  % be sure your image is binary
L = bwlabel(I); % label each object
%Step 2: see the label of each object
s = regionprops(L, 'Centroid');
imshow(I)
hold on

for k = 1:numel(s)
    c = s(k).Centroid;
    text(c(1), c(2), sprintf('%d', k), ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle');
end
hold off
% Step 3: find the area of the object you want using its label
Obj = (L == 3);   % 1 is the label number of the first object. 
figure, imshow(Obj);
Area = regionprops(Obj,'Area') % the answer


I = im2bw (I); %be sure that your image is binary
Total_White_Pixels = nnz(Obj); % total area based on number of white pixels


I = im2bw(I);  % be sure your image is binary
imshow(I)
% Calculate properties of regions in the image and return the data in a
% table.
stats = regionprops('table',I,'Centroid',...
    'MajorAxisLength','MinorAxisLength','Area')

% Get centers and radii of the circles.
centers = stats.Centroid;
diameters = mean([stats.MajorAxisLength stats.MinorAxisLength],2);
radii = diameters/2;
% Plot the circles.
hold on
viscircles(centers,radii);
hold off 