function R = removeShadow(I)
    
    I = rgb2gray(I);
    I1 = imtophat(I, strel('disk',10));
    figure;imshow(I1);
    title('top hat');

    I2 = imadjust(I1);
    figure;imshow(I2);
    title('adjust');

    level = graythresh(I1);
    BW = im2bw(I1,level);
    BW = imdilate(imadjust(im2double(BW)),strel('disk',10));
    figure;imshow(BW);
    title('bw');
    R = I1;
%     C=~BW;
%     figure, imshow(C);
%     D = -bwdist(C);
%     D(C) = -Inf;
%     L = watershed(D);
%     wi = label2rgb(L,'hot','w');
%     figure, imshow(wi);
% 
%     % obj = bwconncomp(L,4);
% 
%     im = I;
%     im(L==0)=0;
%     figure,imshow(im);

end

