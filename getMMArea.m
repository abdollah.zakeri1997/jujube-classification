function [area] = getMMArea(path)
I = imread(path);
I = rgb2gray(I);
I = I < (0.8 * 255);
I = medfilt2(I,[20,20]);
dse = strel('disk',30);
I = imdilate(I,dse);
ese = strel('disk',36);
I = imerode(I,ese);

I = im2bw(I);  % be sure your image is binary
L = bwlabel(I); % label each object
%Step 2: see the label of each object
s = regionprops(L, 'Centroid');
figure;
imshow(I)
hold on

for k = 1:numel(s)
    c = s(k).Centroid;
    text(c(1), c(2), sprintf('%d', k), ...
        'HorizontalAlignment', 'center', ...
        'VerticalAlignment', 'middle');
end
hold off
% Step 3: find the area of the object you want using its label
jujube = (L == 2);   % 1 is the label number of the first object.
box = (L == 1);
figure, imshow(jujube);
jujubeArea = regionprops(jujube,'Area');
jujubeArea = jujubeArea.Area;
boxArea = regionprops(box,'MajorAxisLength','MinorAxisLength'); % the answer
boxArea = boxArea.MajorAxisLength * boxArea.MinorAxisLength;
MMjujubeArea = (jujubeArea / boxArea) * 10000; % convert pixel to sq mil
area = MMjujubeArea;
end

