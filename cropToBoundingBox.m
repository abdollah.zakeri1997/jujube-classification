function croppedImage = cropToBoundingBox(I)
    mask = imbinarize(rgb2gray(I));
    figure;
    imshow(rgb2gray(I));
    
    mask = imfill(~mask,'holes');
    mask = bwareaopen(mask,100);
    figure;
    imshow(mask);
    regions = regionprops(mask);
    maxBoundingBox = [0 0 0 0];
    for k = 1 : numel(regions)
        bb = regions(k).BoundingBox;
        if (bb(3) + bb(4) > maxBoundingBox(3) + maxBoundingBox(4))
            maxBoundingBox = bb;
        end
    end
    croppedImage = imcrop(I,maxBoundingBox);
end

