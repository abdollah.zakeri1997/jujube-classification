function [] = showAll(image,size)
    %original image
    figure;
    subplot(4,4,1);
    imshow(image);
    title('original image');
    
    %gray image
    gray = rgb2gray(image);
    subplot(4,4,2);
    imshow(gray);
    title('gray image');
    
    %adjusted
    adjusted = imadjust(gray);
    subplot(4,4,3);
    imshow(adjusted);
    title('adjusted image');
    
    %binary image
    binary = imbinarize(gray);
    subplot(4,4,4);
    imshow(binary);
    title('binary image');
    
    %complement
    complement = imcomplement(image);
    subplot(4,4,5);
    imshow(complement);
    title('complement image');
    
    %Sobel
    edged = edge(gray,"Sobel");
    subplot(4,4,6);
    imshow(edged);
    title('Sobel edge');
    
    %Prewitt
    edged = edge(gray,"Prewitt");
    subplot(4,4,7);
    imshow(edged);
    title('Prewitt edge');
    
    se = strel('disk',size);
    
    %erode
    eroded = imerode(image,se);
    subplot(4,4,8);
    imshow(eroded);
    title('eroded image');
    
    %dilate
    dilated = imdilate(image,se);
    subplot(4,4,9);
    imshow(dilated);
    title('dilated image');
    
    %reconstruct erode
    Iobr = imreconstruct(eroded,image);
    subplot(4,4,10);
    imshow(Iobr);
    title('erode and reconstruct');
    
    %reconstruct dilate
    Iobr = imreconstruct(dilated,image);
    subplot(4,4,11);
    imshow(Iobr);
    title('dilate and reconstruct');
    
    %open
    opened = imopen(image,se);
    subplot(4,4,12);
    imshow(opened);
    title('opened image');
    
    %close
    closed = imclose(image,se);
    subplot(4,4,13);
    imshow(closed);
    title('closed image');
    
    %reconstruct open
    Iobr = imreconstruct(opened,image);
    subplot(4,4,14);
    imshow(Iobr);
    title('open and reconstruct');
    
    %reconstruct close
    Iobr = imreconstruct(closed,image);
    subplot(4,4,15);
    imshow(Iobr);
    title('close and reconstruct');
    
    %regional minima
    minima = imregionalmin(gray);
    subplot(4,4,15);
    imshow(minima);
    title('regional minima');
    
    %median
    median = medfilt3(image,[size size size]);
    subplot(4,4,16);
    imshow(median);
    title('median filter');
    

end

