function cropped = cropToSample(I)
    mask = imbinarize(rgb2gray(I));
    mask = ~bwareaopen(~mask,200);
    mask = bwareaopen(mask,200);
    figure;
    imshow(~mask);
    labels = bwlabel(mask);
    mask = imopen(labels == 0,strel('disk',50));
    mask = ~bwareaopen(~mask,sum(mask(:)));
    regions = regionprops(mask);
    sampleRegion = regions(1);
    figure;
    imshow(subtractBackground(I));
    cropped = imcrop(subtractBackground(I),sampleRegion.BoundingBox);
end

