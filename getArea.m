function area = getArea(I)
    mask = imbinarize(rgb2gray(I));
    mask = bwareaopen(mask,300);
    mask = ~bwareaopen(~mask,40);
    labels = bwlabel(mask);
    sampleMask = imopen(labels == 0,strel('disk',30));
    sampleRegions = regionprops(sampleMask);
    sampleRegionArea = sampleRegions(1).Area;
    boundingBoxMask = (labels == 2) + sampleMask;

    boundingBoxMask = bwareaopen(boundingBoxMask,100);
    boundingBoxRegions = regionprops(boundingBoxMask);
    boundingBoxRegionArea = boundingBoxRegions(1).Area;
    sampleAreaMilimeters = (sampleRegionArea / boundingBoxRegionArea) * 22500;
    area = sampleAreaMilimeters;
end

