function subtracted = subtractBackground(I)

    dI = im2double(I);
    mask = imbinarize(rgb2gray(I));
    mask = bwareaopen(mask,300);
    mask = ~bwareaopen(~mask,40);
    labels = bwlabel(mask);
    mask = imopen(labels == 0,strel('disk',30));
    mask = ~bwareaopen(~mask,sum(mask(:)));
    subtracted = mask .* dI;

end

